package com.safebear.app;


import com.safebear.app.Pages.UserPage;
import org.junit.Test;

import static org.junit.Assert.assertTrue;

public class Test01_Login extends BaseTest {

    @Test
    public void TestLogin(){

        // 1 Confirm we're on welcome page
        assertTrue(welcomePage.checkCorrectPage());
        // 2 Click on login
        welcomePage.clickOnLogin();
        // 3 Confirm now on login page
        assertTrue(loginPage.checkCorrectPage());
        //4 Login with valid credentials
        loginPage.login("testuser","testing");
        //5 Check that we're now on User Page
        assertTrue(userpage.checkPage());


    }

}

