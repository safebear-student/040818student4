package com.safebear.app;

import com.safebear.app.Pages.LoginPage;
import com.safebear.app.Pages.UserPage;
import com.safebear.app.Pages.WelcomePage;
import org.junit.After;
import org.junit.Before;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public class BaseTest {

    WebDriver driver;
    WelcomePage welcomePage;
    LoginPage loginPage;
    UserPage userpage;


    @Before
    public void setUp() {

        driver = new ChromeDriver();
        welcomePage = new WelcomePage(driver);
        loginPage = new LoginPage(driver);
        userpage = new UserPage(driver);
        driver.get("http://automate.safebear.co.uk/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

    }

    @After
  public void tearDown(){

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        driver.quit();


    }
}

